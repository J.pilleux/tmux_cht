module Main (main) where

import Prompt
import Test.Hspec

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
        describe "append" $ do
                it "should append 'o' to 'tot'" $ do
                        append 'o' "tot" `shouldBe` "toto"

        describe "findChar" $ do
                it "should find 'o' in 'toto'" $ do
                        findChar 'o' "toto" `shouldBe` True
                it "should not find 'i' in 'toto'" $ do
                        findChar 'i' "toto" `shouldBe` False
                it "should not find anything in empty string" $ do
                        findChar 'a' "" `shouldBe` False

        describe "filterPredicate" $ do
                it "should find 'to' in 'toto'" $ do
                        filterPredicate "toto" "to" `shouldBe` True
                it "should find 'ot' in 'toto'" $ do
                        filterPredicate "toto" "ot" `shouldBe` True
                it "should not find 'i' in 'toto'" $ do
                        filterPredicate "toto" "i" `shouldBe` False
                it "should not find 'a' in empty string" $ do
                        filterPredicate "" "a" `shouldBe` False
                it "should return False with empty parameters" $ do
                        filterPredicate "" "a" `shouldBe` False

        describe "formatListSelect" $ do
                it "should change first element" $ do
                        formatListSelect ["a", "b", "c", "d"] 0 `shouldBe` ["  a", "  b", "  c", "» d"]
                it "should change second element" $ do
                        formatListSelect ["a", "b", "c", "d"] 1 `shouldBe` ["  a", "  b", "» c", "  d"]
                it "should change last element" $ do
                        formatListSelect ["a", "b", "c", "d"] 2 `shouldBe` ["  a", "» b", "  c", "  d"]
                it "should not select anything" $ do
                        formatListSelect ["a", "b", "c", "d"] 3 `shouldBe` ["» a", "  b", "  c", "  d"]
                it "should return empty string" $ do
                        formatListSelect [] 0 `shouldBe` []
