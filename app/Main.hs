module Main where

import Prompt (prompt)
import System.IO

main :: IO ()
main = do
  hSetBuffering stdin NoBuffering
  res <- prompt ["One", "Two", "Three", "four"] "" 0
  hSetBuffering stdin LineBuffering

  print res
