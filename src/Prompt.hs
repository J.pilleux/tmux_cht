module Prompt (prompt, append, findChar, filterPredicate, formatListSelect) where

import Data.Char (GeneralCategory (LowercaseLetter, ModifierLetter, OtherLetter, TitlecaseLetter, UppercaseLetter), toLower, generalCategory, isDigit)
import Data.List (intercalate, isInfixOf, length)

import System.Console.ANSI

append :: Char -> String -> String
append a = foldr (:) [a]

lowerString :: String -> String
lowerString = map toLower

findChar :: Char -> String -> Bool
findChar _ [] = False
findChar n (x : xs)
  | x == n = True
  | otherwise = findChar n xs

formatListSelect :: [String] -> Int -> [String]
formatListSelect [] _ = []
formatListSelect [a] n
  | n == 0 = ["» " ++ a]
  | otherwise = ["  " ++ a]
formatListSelect (x : xs) n
  | n == length xs = ("» " ++ x) : formatListSelect xs n
  | otherwise = ("  " ++ x) : formatListSelect xs n

filterPredicate :: String -> String -> Bool
filterPredicate word searchTerm = and bools
  where
    bools = map (`findChar` lowerString word) searchTerm

formatList :: ([String], Int) -> String
formatList (list, index) = intercalate "\n" $ formatListSelect list index

printSearch :: [String] -> String -> Int -> IO ()
printSearch list searchTerm index = do
  let filteredList = filter (`filterPredicate` searchTerm) list
  putStrLn $ formatList (filteredList, index)
  putStrLn $ "> " ++ searchTerm

isLetter :: Char -> Bool
isLetter c = case generalCategory c of
  UppercaseLetter -> True
  LowercaseLetter -> True
  TitlecaseLetter -> True
  ModifierLetter -> True
  OtherLetter -> True
  _ -> False

incrementIndex :: Int -> Int -> Int
incrementIndex index max = (index + 1) `mod` max

decrementIndex :: Int -> Int -> Int
decrementIndex index max = (index - 1) `mod` max

prompt :: [String] -> String -> Int -> IO String
prompt list search index = do
  clearScreen
  let newList = filter (`filterPredicate` search) list
  printSearch list search index
  c <- getChar
  putStrLn ""
  if c == '\DEL' then prompt list (init search) 0
  else if c == '\n' then return $ list !! (((length newList) - 1) - index)
  else if c == 'A' then prompt list search $ incrementIndex index (length newList)
  else if c == 'B' then prompt list search $ decrementIndex index (length newList)
  else if not $ isLetter c then prompt list search index
  else prompt list (append c search) 0
