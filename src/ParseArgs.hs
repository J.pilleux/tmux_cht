module ParseArgs ( getInputFileContent ) where

import Control.Monad
import Data.Char (isSpace)
import Data.List (isPrefixOf)
import Data.Maybe (fromMaybe)
import System.Console.GetOpt
import System.Environment (getArgs)
import System.Exit (exitSuccess)

data Flag
  = Help
  | LanguageFile String
  | ProgramsFile String
  deriving (Show)

data Options = Options
  { optHelp :: Bool,
    optLanguageFile :: IO String,
    optProgramsFile :: IO String
  }

startOptions :: Options
startOptions =
  Options
    { optHelp = False,
      optLanguageFile = getContents,
      optProgramsFile = getContents
    }

options :: [OptDescr (Options -> IO Options)]
options =
  [ Option "h" ["help"] (NoArg (\opt -> return opt {optHelp = True})) "Help of the program",
    Option "l" ["languages"] (ReqArg (\arg opt -> return opt {optLanguageFile = readFile arg}) "FILE") "Input language file",
    Option "p" ["programs"] (ReqArg (\arg opt -> return opt {optProgramsFile = readFile arg}) "FILE") "Input program file"
  ]

printHelpAndExit :: IO ()
printHelpAndExit = do
  putStrLn "Help"
  exitSuccess

getInputFileContent :: IO [String]
getInputFileContent = do
  args <- getArgs
  let (actions, nonOptions, errors) = getOpt RequireOrder options args

  opts <- foldl (>>=) (return startOptions) actions

  let Options
        { optHelp = help,
          optLanguageFile = languages,
          optProgramsFile = programs
        } = opts

  when help printHelpAndExit

  fmap lines programs
  fmap lines languages
